# scripts to merge pc100 data into production

- create a database pc100 next to the osm database
  - import pc100 data from backup
- block editing osm data
- backup osm data
- run [merge.sh](merge.sh) as user with db access: `./merge.sh > merge.log 2>&1`