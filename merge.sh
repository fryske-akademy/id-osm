#!/bin/bash
#
# psql toegang nodig tot pc100 db en productie (web1) db
#
# namen db's
#
pc100="pc100"
osm="osm"

# zet functies klaar
psql < offsets-web1.sql osm
# haal offsets op
o=`echo "select dump_offsets(1000)"|psql osm|grep "[0-9]\{2,\}"`
echo "using offsets: $o"
# bereid functies voor met offsets
gawk -f offsets.awk -v o="$o" dump-pc100.sql > weg.sql
# zet functies klaar
psql < weg.sql pc100

for f in changesets changeset_tags nodes node_tags current_nodes current_node_tags relations relation_tags relation_members current_relations current_relation_members current_relation_tags ways way_nodes  way_tags current_ways current_way_nodes current_way_tags notes note_comments
 do
# dump data voor elke tabel
   echo "select dump_$f();"|psql pc100|gawk -f insert.awk -v t=$f - > ins-$f.sql
# insert data
   psql < ins-$f.sql osm
 done
# zet sequences goed
 psql < seq_web1.sql osm


