BEGIN {
    print "copy " t " from stdin with null as '' CSV;"
    l=""
}
END {
    print l
    print "\\."
}
/\(/ && ! /rows/ {
    if (l!="") print l
#    this may replace starting and ending quotes in for example comment stringsk
    while ($0 ~ /"""/) gsub(/"""/,"\"")
    l=substr($0,3,length($0)-3)
}