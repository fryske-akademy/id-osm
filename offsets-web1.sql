--     \pset null 'null'
-- id's of all parent tables to be imported

drop type offsets cascade;

create type offsets as(ch bigint, curnod bigint, currel bigint, curways bigint, nod bigint,
       note bigint, rel bigint, ways bigint, notecom bigint);

-- way_id, relation_id, node_id, others have auto generated id

CREATE OR REPLACE FUNCTION dump_offsets(divider int)
    RETURNS offsets AS $$
    DECLARE offs offsets%rowtype;
BEGIN
        select into offs
                max(id) + count(*) / divider as ch
        from changesets;
        select into offs
            offs.ch as ch,
            max(id) + count(*) / divider as curnod
        from current_nodes;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            max(id) + count(*) / divider as currel
        from current_relations;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            offs.currel as currel,
            max(id) + count(*) / divider as curways
        from current_ways;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            offs.currel as currel,
            offs.curways as curways,
            max(node_id) + count(*) / divider as nod
        from nodes;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            offs.currel as currel,
            offs.curways as curways,
            offs.nod as nod,
            max(id) + count(*) / divider as note
        from notes;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            offs.currel as currel,
            offs.curways as curways,
            offs.nod as nod,
            offs.note as note,
            max(relation_id) + count(*) / divider as rel
        from relations;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            offs.currel as currel,
            offs.curways as curways,
            offs.nod as nod,
            offs.note as note,
            offs.rel as rel,
            max(way_id) + count(*) / divider as ways
        from ways;
        select into offs
            offs.ch as ch,
            offs.curnod as curnod,
            offs.currel as currel,
            offs.curways as curways,
            offs.nod as nod,
            offs.note as note,
            offs.rel as rel,
            offs.ways as ways,
            max(id) + count(*) / divider as notecom
        from note_comments;
    RETURN offs;
END;
$$  LANGUAGE plpgsql;