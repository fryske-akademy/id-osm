--     \pset null 'null' does not seem to work



drop type offsets cascade;

create type offsets as(ch bigint, curnod bigint, currel bigint, curways bigint, nod bigint,
       note bigint, rel bigint, ways bigint, notecom bigint);

CREATE OR REPLACE FUNCTION userId(id bigint)
    RETURNS bigint AS $$ BEGIN
    RETURN
        CASE id
            WHEN 19 THEN  2 -- rombert
            WHEN  7 THEN  4 -- nienke
            WHEN 10 THEN  5 -- bram
            WHEN  6 THEN  7 -- peter l
            WHEN  2 THEN 27 -- theo
            WHEN  5 THEN 28 -- gerhard
            WHEN 15 THEN 29 -- constantijn
            WHEN  4 THEN 30 -- tjacco
            WHEN  9 THEN 31 -- mark
            WHEN 14 THEN 32 -- ed
            ELSE  1         -- Thomas
            END;
END; $$ LANGUAGE plpgsql;

drop function q(s varchar);
CREATE OR REPLACE FUNCTION q(s varchar)
    RETURNS varchar AS $$ BEGIN
    return concat('"',replace(s, '"','""' ),'"');
END; $$ LANGUAGE plpgsql;

drop function q(v text);
CREATE OR REPLACE FUNCTION q(v text)
    RETURNS text AS $$ BEGIN
    return concat('"',replace(regexp_replace(v, E'[\\n\\r]+', ' ', 'g' ), '"','""' ),'"');
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_nodes()
    RETURNS SETOF nodes AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT node_id + offs.nod,
                        latitude, longitude, changeset_id + offs.ch, visible,
                        timestamp, tile, version, redaction_id
                 from nodes limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_nodes()
    RETURNS SETOF current_nodes AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT id + offs.curnod,
                        latitude, longitude, changeset_id + offs.ch, visible,
                        timestamp, tile, version
                 from current_nodes limit 100;
END; $$ LANGUAGE plpgsql;

drop function dump_changesets();
CREATE OR REPLACE FUNCTION dump_changesets()
    RETURNS SETOF changesets AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT id + offs.ch,
                        userId(user_id), created_at::timestamp, min_lat, max_lat, min_lon, max_lon, closed_at::timestamp, num_changes
                 from changesets limit 100;
END; $$ LANGUAGE plpgsql;

drop function dump_changeset_tags();
CREATE OR REPLACE FUNCTION dump_changeset_tags()
    RETURNS SETOF changeset_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT changeset_id + offs.ch, q(k), q(v)
                 from changeset_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_node_tags()
    RETURNS SETOF node_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT node_id + offs.nod, version, q(k), q(v)
                 from node_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_node_tags()
    RETURNS SETOF current_node_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT node_id + offs.curnod, q(k), q(v)
                 from current_node_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_relations()
    RETURNS SETOF relations AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT relation_id + offs.rel, changeset_id + offs.ch,
                        timestamp, version, visible, redaction_id
                 from relations limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_relations()
    RETURNS SETOF current_relations AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT id + offs.currel, changeset_id + offs.ch,
                        timestamp, visible, version
                 from current_relations limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_relation_members()
    RETURNS SETOF relation_members AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT relation_id + offs.rel, member_type, member_id, member_role, version, sequence_id
                 from relation_members limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_relation_members()
    RETURNS SETOF current_relation_members AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT relation_id + offs.currel, member_type, member_id, member_role, sequence_id
                 from current_relation_members limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_relation_tags()
    RETURNS SETOF relation_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT relation_id + offs.rel, q(k), q(v), version
                 from relation_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_relation_tags()
    RETURNS SETOF current_relation_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT relation_id + offs.currel, q(k), q(v)
                 from current_relation_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_ways()
    RETURNS SETOF ways AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT way_id + offs.ways, changeset_id + offs.ch,
                     timestamp, version, visible, redaction_id
                 from ways limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_ways()
    RETURNS SETOF current_ways AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT id + offs.curways, changeset_id + offs.ch,
                        timestamp, visible, version
                 from current_ways limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_way_nodes()
    RETURNS SETOF way_nodes AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT way_id + offs.ways, node_id + offs.nod,
        version, sequence_id
                 from way_nodes limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_way_nodes()
    RETURNS SETOF current_way_nodes AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT way_id + offs.curways, node_id + offs.curnod, sequence_id
                 from current_way_nodes limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_way_tags()
    RETURNS SETOF way_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT way_id + offs.ways, q(k), q(v), version
                 from way_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_current_way_tags()
    RETURNS SETOF current_way_tags AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT current_way_tags.way_id + offs.curways, q(k), q(v)
                 from current_way_tags limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_notes()
    RETURNS SETOF notes AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT id + offs.note, latitude, longitude, tile, updated_at, created_at, status, closed_at
                 from notes limit 100;
END; $$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dump_note_comments()
    RETURNS SETOF note_comments AS $$ DECLARE offs offsets%rowtype; BEGIN
    offs := #offsets#;
    RETURN query SELECT id + offs.notecom, note_id + offs.note, visible, created_at, author_ip, author_id, q(body), event
                 from note_comments limit 100;
END; $$ LANGUAGE plpgsql;