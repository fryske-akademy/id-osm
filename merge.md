# migratie pc100 - web1

- bepaal voor elke tabel uit db web1 een offset voor de id 
  - see [offsets-web1.sql](offsets-web1.sql)
- lees regels uit tabellen in db pc100 in een file, waarbij
  - see [dump-pc100.sql](dump-pc100.sql)
  - id = id + offset (check grootte)
  - refid = refid + offset voor alle referenties
- lees de files op volgorde in in db web1
  - zie [insert.awk](insert.awk)
  - `echo "select dump_change();"|psql pc100|gawk -f /tmp/insert.awk -v t=changesets - > insertchange.sql`
- zet sequences goed
  - zie [seq_web1.sql](seq_web1.sql)

evt. kan dit volledig in de db gescript als pc100 schema in db web1 komt


Tabellen in 0_osm_db op web1

| schema | tabel                          | type  | merge |
|--------|--------------------------------|-------|-------|
| public | acls                           | table |       |
| public | active_storage_attachments     | table |       |
| public | active_storage_blobs           | table |       |
| public | active_storage_variant_records | table |       |
| public | ar_internal_metadata           | table |       |
| public | changeset_comments             | table |       |
| public | changeset_tags                 | table | VV    |
| public | changesets                     | table | VV    |
| public | changesets_subscribers         | table |       |
| public | client_applications            | table |       |
| public | current_node_tags              | table | VV    |
| public | current_nodes                  | table | VV    |
| public | current_relation_members       | table | VV    |
| public | current_relation_tags          | table | VV    |
| public | current_relations              | table | VV    |
| public | current_way_nodes              | table | VV    |
| public | current_way_tags               | table | VV    |
| public | current_ways                   | table | VV    |
| public | delayed_jobs                   | table |       |
| public | diary_comments                 | table |       |
| public | diary_entries                  | table |       |
| public | diary_entry_subscriptions      | table |       |
| public | friends                        | table |       |
| public | gps_points                     | table |       |
| public | gpx_file_tags                  | table |       |
| public | gpx_files                      | table |       |
| public | issue_comments                 | table |       |
| public | issues                         | table |       |
| public | languages                      | table |       |
| public | messages                       | table |       |
| public | node_tags                      | table | VV    |
| public | nodes                          | table | VV    |
| public | note_comments                  | table | VV    |
| public | notes                          | table | VV    |
| public | oauth_access_grants            | table |       |
| public | oauth_access_tokens            | table |       |
| public | oauth_applications             | table |       |
| public | oauth_nonces                   | table |       |
| public | oauth_tokens                   | table |       |
| public | redactions                     | table |       |
| public | relation_members               | table | VV    |
| public | relation_tags                  | table | VV    |
| public | relations                      | table | VV    |
| public | reports                        | table |       |
| public | schema_migrations              | table |       |
| public | user_blocks                    | table |       |
| public | user_preferences               | table |       |
| public | user_roles                     | table |       |
| public | user_tokens                    | table | VV    |
| public | users                          | table | VV    |
| public | way_nodes                      | table | VV    |
| public | way_tags                       | table | VV    |
| public | ways                           | table | VV    |
